//
//  LocalDataProvider.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import Foundation
struct LocalDataProvider {
    
    static func saveToken(token:String){
        UserDefaults.standard.setValue(token, forKey: "USER_TOKEN")
    }
    
    static func token() -> String {
        return UserDefaults.standard.string(forKey: "USER_TOKEN") ?? ""
    }
    
    static func saveUser(user:UserModel){
        if let data = try? NSKeyedArchiver.archivedData(withRootObject: user, requiringSecureCoding: false) {
            UserDefaults.standard.set(data, forKey: "USER_MODEL")
        }
    }
    static func getUser() -> UserModel {
        if let savedData = UserDefaults.standard.object(forKey: "USER_MODEL") as? Data {
            if let decodedPerson = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(savedData) as? UserModel {
                let person = decodedPerson
                return person
            }
        }
        return UserModel(id: "", age: "", phone: "", email: "", gender: "", name: "", avatar: "")
    }
    
    static func removeUser(){
        UserDefaults.standard.removeObject(forKey: "USER_MODEL")
        UserDefaults.standard.removeObject(forKey: "USER_TOKEN")
        UserDefaults.standard.synchronize()
    }
}
