//
//  Enum.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/12/21.
//

import Foundation

enum ResultFirebase {
    case success
    case err(Error)
}
enum ResulProduct {
    case success([ProductModel])
    case err(Error)
}

enum ResultImageUrl{
    case success(URL)
    case err(Error)
}
enum ResultShop{
    case success([UserProduct])
    case err(Error)
}

enum ResultUser{
    case success(UserModel)
    case err(Error)
}

enum Gender : String{
    case male = "Nam"
    case female = "Nữ"
    case other = ""
}

enum TypeProduct: String {
    case new = "NEW"
    case now = "NOW"
}
