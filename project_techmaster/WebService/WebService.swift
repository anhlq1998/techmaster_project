//
//  WebService.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/12/21.
//

import Foundation
class WebService {
    static func parseUser(dict:[String:Any]) -> UserModel {
        let user = UserModel()
        user.id = dict["id"] as! String
        user.name = dict["name"] as! String
        user.age = dict["age"] as! String
        user.phone = dict["phone"] as! String
        user.email = dict["email"] as! String
        user.gender = dict["gender"] as! String
        user.avatar = dict["avatar"] as! String
        return user
    }
}
