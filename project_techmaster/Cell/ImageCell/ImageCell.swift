//
//  ImageCell.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/7/21.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var imgImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension ImageCell {
    static var cell : String {
        get {
            return "ImageCell"
        }
    }
}
