//
//  ImagePikerCell.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/12/21.
//

import UIKit


protocol ImagePikerCellDelegate {
    func deleteItem(index:Int)
}
class ImagePikerCell: UICollectionViewCell {
    var delegate:ImagePikerCellDelegate!
    var index : Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnDelete(_ sender: Any) {
//        print(index)
        delegate.deleteItem(index: index)
    }
}

extension ImagePikerCell {
    static var cell : String {
        get {
            return "ImagePikerCell"
        }
    }
}
