//
//  CommentCell.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/6/21.
//

import UIKit

class CommentCell: UITableViewCell {
    @IBOutlet var lbComment: UILabel!
    @IBOutlet var lbUser: UILabel!
    @IBOutlet var lbAvatar: UIImageView!
    @IBOutlet var lbTime: UILabel!
    @IBOutlet var btnEditComment: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(model:MessageModel){
        lbComment.text = model.comment
        lbTime.text = model.time
        lbUser.text = model.user
        print("MODEL", model.idUSer)
        print("Local", LocalDataProvider.getUser().id)
        if model.idUSer == LocalDataProvider.getUser().id{
            btnEditComment.isHidden = false
        }else{
            btnEditComment.isHidden = true
        }
    }
    
    @IBAction func btnEditComment(_ sender: Any) {
    }
}

extension CommentCell {
    static var cell : String {
        get {
            return "CommentCell"
        }
    }
}
