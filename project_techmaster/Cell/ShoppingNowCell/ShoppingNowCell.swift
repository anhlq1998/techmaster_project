//
//  ShoppingNowCell.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

class ShoppingNowCell: UICollectionViewCell {
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var lbTitle: UILabel!
    @IBOutlet var lbPrice: UILabel!
    @IBOutlet var imgFavorite: UIImageView!
    @IBOutlet var imageNew: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func layoutSubviews() {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateHomeShopNow(model:ProductModel){
        lbTitle.text = model.name
        lbPrice.text = "\(model.price) $"
        getImageFromUrl(url: model.image.first ?? "", image: imgProduct)
        self.checkType(model: model)
    }
    
    func updateHomeShopNew(model:ProductModel){
        lbTitle.text = model.name
        lbPrice.text = "\(model.price) $"
        getImageFromUrl(url: model.image.first ?? "", image: imgProduct)
        self.checkType(model: model)
        
    }
    
    func updateMyShop(model:UserProduct){
        lbTitle.text = model.name
        lbPrice.text = "\(model.price) $"
        getImageFromUrl(url: model.image.first ?? "", image: imgProduct)
//        self.checkType(model: model)
    }
    
    func checkType(model:ProductModel){
        if model.type == TypeProduct.new.rawValue {
            imageNew.image = UIImage(named: "ic_new")
        }
    }

}

extension ShoppingNowCell {
    static var cell : String {
        get {
            return "ShoppingNowCell"
        }
    }
}
