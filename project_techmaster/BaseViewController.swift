//
//  BaseViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import Foundation
import UIKit
class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.view.backgroundColor = UIColor.white
//        self.navigationController?.navigationBar.tintColor = .shMainColor
        
        self.createEvent()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fecthAPI()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupLayout()
    }
    
    func createEvent(){
        
    }
    
    func setupLayout(){
        
    }
    
    func fecthAPI(){
        
    }
}
