//
//  CommentViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/6/21.
//

import UIKit

class CommentViewController: BaseViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewPopup: UIView!
    var arrMessage : [MessageModel] = []
    @IBOutlet var viewBorderMessage: UIView!
    @IBOutlet var heightMessage: NSLayoutConstraint!
    @IBOutlet var tvMessage: UITextView!
    var product:ProductModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        self.setupTextView()
        tableView.keyboardDismissMode = .onDrag
        guard let p = product else {return}
        FirebaseManager.writeMessage(comment: "shdkajsdhajks h     ", idProduct: p.idproduct)
        
    }
    override func viewWillLayoutSubviews() {
        viewPopup.clipsToBounds = true
        viewPopup.layer.cornerRadius = 20
        viewPopup.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        viewBorderMessage.clipsToBounds = true
        viewBorderMessage.layer.borderWidth = 0.3
        viewBorderMessage.layer.borderColor = UIColor.lightGray.cgColor
        viewBorderMessage.layer.cornerRadius = 10
        
    }
    
    func setupTextView(){
        tvMessage.delegate = self
    }
    
    func setupAutosize(){
        heightMessage.constant = tvMessage.contentSize.height
    }

    
    override func createEvent() {
        tableView.isUserInteractionEnabled = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: CommentCell.cell, bundle: nil), forCellReuseIdentifier: "cell")
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hidePopup)))
        NotificationCenter.default.addObserver(self,
                                                      selector: #selector(keyboardWillShow(notification:)),
                                                      name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self,
                                                      selector: #selector(keyboardWillHide(notification:)),
                                                      name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {

        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        self.view.frame.origin.y -= keyboardSize.height
            }
        }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        }
        
    }
    
    
    
    @objc func hidePopup(){
        DispatchQueue.main.async {
            self.view.backgroundColor = .clear
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}

extension CommentViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentCell
        cell.updateCell(model: arrMessage[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

extension CommentViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height >= 150 {
            heightMessage.constant = 150
            return
        }
        if textView.contentSize.height <= 30 {
            heightMessage.constant = 30
            return
        }
        self.setupAutosize()
    }
}
