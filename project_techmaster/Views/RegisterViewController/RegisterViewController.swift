//
//  RegisterViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

class RegisterViewController: BaseViewController {
    @IBOutlet var tfName: UITextField!
    @IBOutlet var tfPhone: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var tfBirthday: UITextField!
    @IBOutlet var imgMale: UIImageView!
    @IBOutlet var imgFemale: UIImageView!
    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var lbLogin: UILabel!
    
    var gender = Gender.other
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func createEvent() {
        self.settingLabelLogin()
        self.setupGesture()
    }


    @IBAction func btnRegister(_ sender: Any) {
        self.registerEmai()
    }
}
extension RegisterViewController {
    func settingLabelLogin(){
        let text = "Bạn đã có tài khoản? đăng nhập tại đây"
        lbLogin.text = text
        self.lbLogin.textColor =  UIColor.black
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "tại đây")
        if #available(iOS 13.0, *) {
            underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.link, range: range1)
        } else {
            // Fallback on earlier versions
        }
        lbLogin.attributedText = underlineAttriString
        lbLogin.isUserInteractionEnabled = true
        lbLogin.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(ationLabelRegister(gesture:))))

    }
    @objc func ationLabelRegister(gesture: UITapGestureRecognizer){
        let text = "Bạn đã có tài khoản? đăng nhập tại đây"
        let termsRange = (text as NSString).range(of: "tại đây")

        if gesture.didTapAttributedTextInLabel(label: lbLogin, inRange: termsRange) {
            navigationController?.popViewController(animated: true)
            return
       }
    }
    func registerEmai(){
        guard let name = tfName.text else {return}
        guard let emai = tfEmail.text else {return}
        guard let password = tfEmail.text else {return}
        guard let phone = tfPhone.text else {return}
        guard let age = tfBirthday.text else {return}
        
        
        if emai.isEmpty || password.isEmpty || phone.isEmpty && age.isEmpty || name.isEmpty {
            self.showAlert(title: "Thông báo", message: "Bạn không được để trống textField")
            return
        }
        if isValidEmail(emai) == false {
            self.showAlert(title: "Thông báo", message: "Không đúng định dạng email")
            return
        }
        if password.count < 4 {
            self.showAlert(title: "Thông báo", message: "Mật khẩu không được nhỏ hơn 4 ký tự")
            return
        }
        if self.gender.rawValue == Gender.other.rawValue{
            self.showAlert(title: "Thông báo", message: "Bạn chưa chọn giới tính")
            return
        }
        if isValidPhone(phone: phone) == false {
            self.showAlert(title: "Thông báo", message: "Số điện thoại không hợp lệ")
            return
        }
        
        
        let id = UUID().uuidString
        FirebaseAuth.registerEmaiPassword(id, name, age: age, phone, self.gender.rawValue, emai, password) { (result) in
            switch result {
            case .success :
                self.showAlert(title: "Thông báo", message: "Bạn đã đăng ký thành công")
            case . err(let err):
                self.showAlert(title: "Thông báo", message: "\(err.localizedDescription)")
            }
        }
        
    }
    func setupGesture(){
        imgMale.isUserInteractionEnabled = true
        imgFemale.isUserInteractionEnabled = true
        
        imgMale.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(setGenderMale)))
        imgFemale.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(setGenderFamele)))
        
        
    }
    
    @objc func setGenderMale(){
        self.gender = Gender.male
        self.updateGender()
    }
    @objc func setGenderFamele(){
        self.gender = Gender.female
        self.updateGender()
    }
    
    func updateGender(){
        if gender.rawValue == Gender.male.rawValue {
            DispatchQueue.main.async {
                self.imgMale.image = UIImage(named: "ic_radio_filled")
                self.imgFemale.image = UIImage(named: "ic_radio")
            }
            
            return
        }
        
        if gender.rawValue == Gender.female.rawValue {
            DispatchQueue.main.async {
                self.imgFemale.image = UIImage(named: "ic_radio_filled")
                self.imgMale.image = UIImage(named: "ic_radio")
            }
            
            return
        }
        
    }
    
}
