//
//  LoginViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit
import FirebaseAuth
class LoginViewController: UIViewController {
    
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPassword: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var lbRegister: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingLabelRegister()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(LocalDataProvider.getUser().id,"TOKEN")
        
    }
    
    
    
    @IBAction func btnLogin(_ sender: Any) {
        loginWithEmai()

        
    }
    
    @IBAction func btnLoginFB(_ sender: Any) {
        
    }
    
    @IBAction func btnLoginGoogle(_ sender: Any) {
        
    }
    func settingLabelRegister(){
        let text = "Bạn chưa có tài khoản? đăng nhập tại đây"
        lbRegister.text = text
        self.lbRegister.textColor =  UIColor.black
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "tại đây")
        if #available(iOS 13.0, *) {
            underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.link, range: range1)
        } else {
            // Fallback on earlier versions
        }
        lbRegister.attributedText = underlineAttriString
        lbRegister.isUserInteractionEnabled = true
        lbRegister.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(ationLabelRegister(gesture:))))

    }
    @objc func ationLabelRegister(gesture: UITapGestureRecognizer){
        let text = "Bạn chưa có tài khoản? đăng nhập tại đây"
        let termsRange = (text as NSString).range(of: "tại đây")

        if gesture.didTapAttributedTextInLabel(label: lbRegister, inRange: termsRange) {
            let vc = RegisterViewController()
            navigationController?.pushViewController(vc, animated: true)
            return
       }
    }
    
    func loginWithEmai(){
        guard let emai = tfEmail.text else {return}
        guard let password = tfEmail.text else {return}
        
        if emai.isEmpty && password.isEmpty {
            self.showAlert(title: "Thông báo", message: "Bạn không được để trống email hoặc password")
            return
        }
        if isValidEmail(emai) == false {
            self.showAlert(title: "Thông báo", message: "Không đúng định dạng email")
            return
        }
        if password.count < 4 {
            self.showAlert(title: "Thông báo", message: "Mật khẩu không được nhỏ hơn 4 ký tự")
            return
        }
        FirebaseAuth.loginEmailPassword(emai, password) { (result) in
            switch result {
            case .success:
                AppDelegate.getListProduct()
                FirebaseAuth.isLoginUser(email: emai)
                let vc = TabbarViewController()
                self.navigationController?.pushViewController(vc, animated: true)
                //
            case .err(let err):
                self.showAlert(title: "Thông báo", message: "\(err.localizedDescription)")
            }
        }
        
    }
    
}
