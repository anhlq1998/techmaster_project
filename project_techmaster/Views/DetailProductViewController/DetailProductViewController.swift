//
//  DetailProductViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

class DetailProductViewController: BaseViewController {
//    @IBOutlet var imgProdcut: UIImageView!
    @IBOutlet var collectionViewImage: UICollectionView!
    @IBOutlet var lbTitle: UILabel!
    @IBOutlet var lbdescription: UILabel!
    @IBOutlet var lbPrice: UILabel!
    @IBOutlet var collectionViewRelatedProducts: UICollectionView!
    var product:ProductModel?
    var arrProduct : [ProductModel] = []
    var arrComment : [MessageModel] = []
    @IBOutlet var btnShowMessage: UIButton!
    @IBOutlet var lbMessage: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()

//        getImageFromUrl(url: product!.image, image: imgProdcut)
        lbPrice.text = product!.price + " $"
        lbdescription.text = product!.description
        lbTitle.text = product!.name
        guard let message = product?.message else {return}
        self.fetchMessage(message: message)
        self.getInfoShop()
        
      
        
        // Do any additional setup after loading the view.
    }
    override func createEvent() {
        collectionViewRelatedProducts.delegate = self
        collectionViewRelatedProducts.dataSource = self
        collectionViewRelatedProducts.register(UINib(nibName: ShoppingNowCell.cell, bundle: nil), forCellWithReuseIdentifier: "cell")
        arrProduct = FirebaseManager.products.filter({$0.idproduct != product?.idproduct})
        
        self.settingPageControl(count: product!.image.count)
        
        collectionViewImage.delegate = self
        collectionViewImage.dataSource = self
        collectionViewImage.register(UINib(nibName: ImageCell.cell, bundle: nil), forCellWithReuseIdentifier: "imageCell")
        collectionViewImage.bounces = false
        collectionViewImage.isPagingEnabled = true
        
    }
    
    func settingPageControl(count:Int){
        pageControl.hidesForSinglePage = true
        pageControl.numberOfPages = count
    }
    
    func fetchMessage(message:[[String:Any]]){
        self.lbMessage.text = "Bình luận (\(message.count))"
        if message.count == 0 {
            btnShowMessage.setTitle("Viết bình luận", for: .normal)
        }else{
            btnShowMessage.setTitle("Xem tất cả", for: .normal)
        }
        for i in message {
            guard let comment = i["comment"] else {return}
            guard let idUSer = i["idUSer"] else {return}
            guard let time = i["time"] else {return}
            guard let user = i["user"] else {return}
            let ms = MessageModel(user: user as! String, idUser: idUSer as! String, time: time as! String, comment: comment as! String)
            self.arrComment.append(ms)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.setCustomNavigationbar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @IBAction func btnAddtoCart(_ sender: Any) {
        self.showToast(text: "Thêm vào giỏ hàng thành công")

    }
    @IBAction func btnShowComment(_ sender: Any) {
        print("Show comment")
        let vc = CommentViewController()
        vc.product = product
        vc.arrMessage = arrComment
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    func getInfoShop(){
        guard let p = product else {return}
        FirebaseAuth.getUserShop(idWriter: p.idWriter) { (resut) in
            switch resut {
            case .success(let user):
                print(user.name)
            case .err(_):
                break
            }
        }
    }
    
}



extension DetailProductViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewImage {
            return (product?.image.count)!
        }
        return arrProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // collectionview image
        if collectionView == collectionViewImage {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! ImageCell
            let image = product!.image[indexPath.row]
            print("Image---\(image)")
            getImageFromUrl(url: image, image: cell.imgImage)
            return cell
        }
        
        //collection view shop
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ShoppingNowCell
        cell.updateHomeShopNow(model: arrProduct[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // collectionview image
        if collectionView == collectionViewImage {
            return CGSize(width: collectionViewImage.frame.width, height: collectionViewImage.frame.height)
        }
        //
        return CGSize(width: (collectionViewRelatedProducts.frame.width - 15) / 2.5 , height: collectionViewRelatedProducts.frame.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewImage {
            return 0
        }
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionViewImage {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right:  0)
        }
        //
        return UIEdgeInsets(top: 0, left: 15, bottom: 10, right: 15)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewImage{
            
        }else{
            let vc = DetailProductViewController()
            vc.product = arrProduct[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contains(collectionViewImage){
            let offSet = scrollView.contentOffset.x
                let width = scrollView.frame.width
                let horizontalCenter = width / 2
                pageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
        }
    }
    
    
}
