//
//  ProfileViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet var imgAvatar: UIImageView!
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbBirthday: UILabel!
    
    @IBOutlet var viewHistory: UIView!
    @IBOutlet var viewFavorite: UIView!
    
    @IBOutlet var viewHelp: UIView!
    @IBOutlet var viewSetting: UIView!
    @IBOutlet var viewLogout: UIView!
    
    @IBOutlet var viewMyShop: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createEvent()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
//        self.getUser()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getUser()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        imgAvatar.layer.cornerRadius = imgAvatar.frame.width / 2
        imgAvatar.clipsToBounds = true
    }
    

    
    
    
    func createEvent(){
        self.addGestureView()
        
    }
    
    
    func getUser(){
        let user = LocalDataProvider.getUser()
        lbName.text = user.name
        lbBirthday.text = user.age
        if user.avatar == "" {
            if user.gender == Gender.male.rawValue {
                imgAvatar.image = UIImage(named: "avatar_male")
            }else{
                imgAvatar.image = UIImage(named: "avatar_famale")
            }
        }else {
            getImageFromUrl(url: user.avatar, image: imgAvatar)
        }
    }
    
    func addGestureView(){
        imgAvatar.isUserInteractionEnabled = true
        let gestureAvatar = UITapGestureRecognizer(target: self, action: #selector(gotoDetailProfile))
        imgAvatar.addGestureRecognizer(gestureAvatar)
        
        
        viewLogout.isUserInteractionEnabled = true
        viewLogout.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoLogout)))
        
        viewMyShop.isUserInteractionEnabled = true
        viewMyShop.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToMyShop)))
    }
    
    @objc func goToMyShop(){
        let vc = MyShopViewController()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func gotoDetailProfile(){
        print("Go to detail")
        let vc = DetailProfileViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func gotoLogout(){
        self.showCustomAlert(title: "Thông báo", message: "Bạn có muốn đăng xuất", titleAction: "Đăng xuất") { () -> (Void) in
            LocalDataProvider.removeUser()
            FirebaseAuth.logout()
            let vc = LoginViewController()
            let nav = UINavigationController(rootViewController: vc)
            let window = UIApplication.shared.keyWindow
            window?.rootViewController = nav
            window?.makeKeyAndVisible()
        }
    }
    
    

}
