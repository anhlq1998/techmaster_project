//
//  HomeShopNowViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/4/21.
//

import UIKit

class HomeShopNowViewController: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    var product = [ProductModel]()
    var navigation = UINavigationController()
    var viewcontroller = UIViewController()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("HomeShopNowViewController", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.createEvent()
    }
    func createEvent(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: ShoppingNowCell.cell, bundle: nil), forCellWithReuseIdentifier: "cell")
        NotificationCenter.default.addObserver(self, selector: #selector(getProducts), name: .noti_get_products, object: nil)
        
    }
    @objc func getProducts(){
        self.product = FirebaseManager.products.filter({$0.type == TypeProduct.now.rawValue})
        print("Procut NOW \(product.count)")
        self.collectionView.reloadData()
    }
    
    
}

extension HomeShopNowViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ShoppingNowCell
        cell?.updateHomeShopNow(model: product[indexPath.row])
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width  - 20) / 2 , height: (collectionView.frame.height  - 16) / 2 )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        // |cell-0-cell| return 0
        // |cell-10-cell| return 10
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let p = product[indexPath.row]
        let vc = DetailProductViewController()
        vc.product = p
        vc.hidesBottomBarWhenPushed = true
        navigation.pushViewController(vc, animated: true)
    }
        

}
