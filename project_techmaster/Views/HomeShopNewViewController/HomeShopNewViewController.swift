//
//  HomeShopNewViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/4/21.
//

import UIKit

class HomeShopNewViewController: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var collectionview: UICollectionView!
    var viewController : UIViewController = UIViewController()
    var navigation : UINavigationController = UINavigationController()
    var product = [ProductModel]()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("HomeShopNewViewController", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.createEvent()
        
    }
    func createEvent(){
        collectionview.reloadData()
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.register(UINib(nibName: ShoppingNowCell.cell, bundle: nil), forCellWithReuseIdentifier: "cell")
        NotificationCenter.default.addObserver(self, selector: #selector(getProducts), name: .noti_get_products, object: nil)
    }
    @objc func getProducts(){
        self.product = FirebaseManager.products.filter({$0.type == TypeProduct.new.rawValue})
        print("PRODUCT \(FirebaseManager.products.count)")
        self.collectionview.reloadData()
    }

}
extension HomeShopNewViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.product.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ShoppingNowCell
        cell.updateHomeShopNew(model: product[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionview.frame.width - 5) / 2 , height: (collectionview.frame.height - 16) / 2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailProductViewController()
        vc.product = product[indexPath.row]
        navigation.pushViewController(vc, animated: true)
    }
    
    
    
}
