//
//  DetailProfileViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

class DetailProfileViewController: BaseViewController {
    @IBOutlet var tfUserName: UITextField!
    @IBOutlet var tfBirthday: UITextField!
    @IBOutlet var tfPhone: UITextField!
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var imgMale: UIImageView!
    @IBOutlet var imgFemale: UIImageView!
    @IBOutlet var imgAvatar: UIImageView!
    @IBOutlet var imgChooseImage: UIImageView!
    
    var type : Gender = Gender(rawValue: Gender.other.rawValue)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func createEvent() {
        self.updateUser()
        self.addGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.setCustomNavigationbar()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        imgAvatar.layer.cornerRadius = imgAvatar.frame.width / 2
        imgAvatar.clipsToBounds = true
    }
    
    func updateUser(){

        let user = LocalDataProvider.getUser()
        if user.gender == Gender.male.rawValue {
            imgMale.image = UIImage(named: "ic_radio_filled")
            imgFemale.image = UIImage(named: "ic_radio")
        }else{
            imgMale.image = UIImage(named: "ic_radio")
            imgFemale.image = UIImage(named: "ic_radio_filled")
        }
        self.type = Gender(rawValue: user.gender)!
        tfUserName.text = user.name
        tfBirthday.text = user.age
        tfPhone.text = user.phone
        tfEmail.text = user.email
        if user.avatar == "" {
            if user.gender == Gender.male.rawValue {
                imgAvatar.image = UIImage(named: "avatar_male")
            }else{
                imgAvatar.image = UIImage(named: "avatar_famale")
            }
        }else {
            getImageFromUrl(url: user.avatar, image: imgAvatar)
        }
    }
    
    func addGestureView(){
        imgMale.isUserInteractionEnabled = true
        imgFemale.isUserInteractionEnabled = true
        imgMale.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chosseMale)))
        imgFemale.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseFamale)))
        
        imgChooseImage.isUserInteractionEnabled = true
        imgChooseImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(chooseImage)))
    }
    
    @objc func chosseMale(){
        imgMale.image = UIImage(named: "ic_radio_filled")
        imgFemale.image = UIImage(named: "ic_radio")
        self.type = .male
    }
    
    
    @objc func chooseFamale(){
        imgMale.image = UIImage(named: "ic_radio")
        imgFemale.image = UIImage(named: "ic_radio_filled")
        self.type = .female
    }
    
    
    
    @IBAction func btnUpdate(_ sender: Any) {
        guard let name = tfUserName.text else{return}
        guard let age = tfBirthday.text else {return}
        guard let phone = tfPhone.text else {return}
        
        
        if name.isEmpty || age.isEmpty || phone.isEmpty {
            self.showAlert(title: "Thông báo", message: "Bạn không được để trống text")
            return
        }
        if isValidPhone(phone: phone) == false {
            self.showAlert(title: "Thông báo", message: "Số điện thoại không hợp lệ")
            return
        }
        let path = LocalDataProvider.getUser().id + "_" + "avatar"
        
        FirebaseManager.upLoadImage(image: (imgAvatar.image ?? UIImage(named: "img_demo"))!,filePath: path) { (resutl) in
            switch resutl {
            case .success(let url):
                
                FirebaseAuth.updateUser(name: name, age: age, phone: phone, gender: self.type.rawValue, avatar: url.absoluteString) { (result) in
                    switch result {
                    
                    case.success :
                        let user = LocalDataProvider.getUser()
                        user.age = age
                        user.phone = phone
                        user.gender = self.type.rawValue
                        user.name = name
                        user.avatar = url.absoluteString
                        LocalDataProvider.saveUser(user: user)
                        self.navigationController?.popViewController(animated: true)
                        
                    case .err(let _ ):
                        self.showAlert(title: "Thông báo", message: "Cập nhật thất bại")
                    
                    }
                }
                
            case .err(let err ):
                self.showAlert(title: "Thông báo", message: "Cập nhật thất bại")
            }
        }

        
        
    }

    
    
    
    @objc func chooseImage(){
        print("image piker")
        print("Piker Here")
        let p = UIImagePickerController()
        p.delegate = self
        p.allowsEditing = true
//        p.mediaTypes = ["public.image"]
        p.sourceType = .photoLibrary
        present(p, animated: true, completion: nil)
    }
}

extension DetailProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(picker)
        guard let image = info[.editedImage] as? UIImage else {return}
        imgAvatar.image = image
        self.dismiss(animated: true, completion: nil)
//        FirebaseManager.upLoadImage(image: imgAvatar.image!, completion: <#FirebaseManager.completionResultUrlImage#>)
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        print("Cancle")
    }
    
}
