//
//  SearchViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

class SearchViewController: BaseViewController {
    
    let searchbar = UISearchBar()
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = searchbar
        searchbar.delegate = self
        searchbar.becomeFirstResponder()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setCustomNavigationbar(action:#selector(handleBack))
        self.showRightbarButton()
        
    }
    override func createEvent() {
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: SearchProductItemCell.cell, bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    func showRightbarButton(){
        let rightBar = UIBarButtonItem(customView: UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 20)))
        navigationItem.rightBarButtonItem = rightBar
    }
    @objc func handleBack(){
        tabBarController?.selectedIndex = 0
    }
    
}

extension SearchViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SearchProductItemCell
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}
extension SearchViewController : UISearchBarDelegate {
    
}
