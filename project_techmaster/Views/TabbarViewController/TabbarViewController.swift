//
//  TabbarViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import Foundation
import UIKit

class TabbarViewController : UITabBarController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vLogin = HomeViewController()
        let navLogin = UINavigationController(rootViewController: vLogin)
        let itemLogin = UITabBarItem(title: "home", image: UIImage(named: "ic_home")?.withRenderingMode(.alwaysTemplate), tag: 0)
        navLogin.tabBarItem = itemLogin
        
        
        let vSearch = SearchViewController()
        let navSearch = UINavigationController(rootViewController: vSearch)
        let itemSearch = UITabBarItem(title: "search", image: UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate), tag: 1)
        navSearch.tabBarItem = itemSearch
        
        
        let vNoti = NotificationViewController()
        let navNoti = UINavigationController(rootViewController: vNoti)
        let itemNoti = UITabBarItem(title: "notication", image: UIImage(named: "ic_notification")?.withRenderingMode(.alwaysTemplate), tag: 2)
        navNoti.tabBarItem = itemNoti
        
        
        let vProfile = ProfileViewController()
        let navProfile = UINavigationController(rootViewController: vProfile)
        let itemProfile = UITabBarItem(title: "profile", image: UIImage(named: "ic_user")?.withRenderingMode(.alwaysTemplate), tag: 3)
        navProfile.tabBarItem = itemProfile
        
        self.tabBar.isTranslucent = false
        self.tabBar.barTintColor = .white
        self.tabBar.tintColor = .brown
        self.viewControllers = [navLogin,navProfile]
        
    }
}
extension UIImage {

    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

}
