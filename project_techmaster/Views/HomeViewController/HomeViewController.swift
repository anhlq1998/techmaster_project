//
//  HomeViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

class HomeViewController: BaseViewController {
    @IBOutlet var collectionViewBanner: UICollectionView!
    @IBOutlet var view1: HomeShopNowViewController!
    @IBOutlet var view2: HomeShopNewViewController!
    @IBOutlet var view3: UIView!
    @IBOutlet var view4: UIView!
    @IBOutlet var tfSearch: UITextField!
    @IBOutlet var viewSearch: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSearch.delegate = self
        tfSearch.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTabTextField)))
        
    }
    override func viewWillLayoutSubviews() {
        viewSearch.layer.cornerRadius = 10
    }
    
    override func createEvent() {
        view1.viewcontroller = self
        view1.navigation = navigationController!
        
        view2.viewController = self
        view2.navigation = navigationController!
        
        
        
        
      
    
        
    }
    @objc func didTabTextField(){
        self.tabBarController?.selectedIndex = 1
        tfSearch.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
}

extension HomeViewController : UITextFieldDelegate {
    
}
