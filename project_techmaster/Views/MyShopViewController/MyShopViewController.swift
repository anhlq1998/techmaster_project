//
//  MyShopViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/11/21.
//

import UIKit

class MyShopViewController: BaseViewController {
    var arrProduct = [UserProduct]()
    @IBOutlet var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func createEvent() {
        self.registerCollectionView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchData()
    }
    
    func fetchData(){
        FirebaseManager.getMyShop { (resut) in
            switch resut {
            case .success(let product):
                self.arrProduct = product
                self.collectionView.reloadData()
                
            case .err(let err):
                print("MY SHOP ERR \(err)")
            }
        }
        
    }
    
    
    func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: ShoppingNowCell.cell, bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.setCustomNavigationbar()
    }
    
    
    @IBAction func btnAddProduct(_ sender: Any) {
//        FirebaseManager.createProduct(name: "Test \(Int.random(in: 0...3))", description: "demo \(Int.random(in: 1...100))", price: "100", image: ["https://firebasestorage.googleapis.com/v0/b/projecttechmaster.appspot.com/o/BCDAAA0B-B001-4A01-A2EE-79E589514EF0?alt=media&token=45707570-5f98-49a1-8975-ca8597d3ce03"], type: "NEW") { (result) in
//            switch result {
//
//            case .success:
//                self.fetchData()
//                AppDelegate.getListProduct()
//            case .err(_):
//                break
//            }
//        }
//        self.collectionView.reloadData()
        let vc = AddProductViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MyShopViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ShoppingNowCell
        cell.updateMyShop(model: arrProduct[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((collectionView.frame.width - 24 ) / 2), height: (collectionView.frame.height / 3 ))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 7)
    }
    
    
}
