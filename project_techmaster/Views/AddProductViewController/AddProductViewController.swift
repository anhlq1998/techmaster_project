//
//  AddProductViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/12/21.
//

import UIKit

class AddProductViewController: BaseViewController {
    @IBOutlet var tfTitle: UITextField!
    @IBOutlet var tfprice: UITextField!
    @IBOutlet var tfType: UITextField!
    @IBOutlet var tfImage: UITextField!
    @IBOutlet var tvDescription: UITextView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var viewImage: UIView!
    @IBOutlet var textViewHeight: NSLayoutConstraint!
    var arr :[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTextView()
        

        
    }
    override func createEvent() {
        self.registerCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setCustomNavigationbar()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        viewImage.layer.cornerRadius = 5
        viewImage.clipsToBounds = true
        viewImage.layer.borderWidth = 0.3
        viewImage.layer.borderColor = UIColor.lightGray.cgColor
        
        tvDescription.layer.cornerRadius = 5
        tvDescription.clipsToBounds = true
        tvDescription.layer.borderWidth = 0.3
        tvDescription.layer.borderColor = UIColor.lightGray.cgColor
        
    }

    @IBAction func btnAddProduct(_ sender: Any) {
        arr.removeAll()
        collectionView.reloadData()
        self.setCollectionViewHidden()
    }
    func setupTextView(){
        self.setupPlaceHolderTextView()
        tvDescription.delegate = self
        
        
    }
    func setupAutosizeTextView(){
        textViewHeight.constant = tvDescription.contentSize.height
        print(tvDescription.contentSize.height)
    }
    
    func setupPlaceHolderTextView(){
        tvDescription.text = "Nhập thông tin chi tiết sản phẩm "
        tvDescription.textColor = .lightGray
    }
    
    @IBAction func btnAddImage(_ sender: Any) {
        if arr.count == 5 {
            self.showAlert(title: "Thông báo", message: "Chỉ được phép nhập tối đa 5 ảnh")
            return
        }
        arr.append("\(Int.random(in: 0...100))")
        self.setCollectionViewHidden()
    }
    
    func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: ImagePikerCell.cell, bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    
    func setCollectionViewHidden(){
        collectionView.isHidden = arr.count > 0 ? false : true
        tfImage.isHidden = arr.count > 0 ? true : false
        collectionView.reloadData()
    }
}


extension AddProductViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImagePikerCell
        cell.index = arr.firstIndex(of: arr[indexPath.row])!
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 4), height: (collectionView.frame.height - 20))
    }
    
    
}

extension AddProductViewController : ImagePikerCellDelegate {
    func deleteItem(index: Int) {
        print(index)
        self.arr.remove(at: index)
        self.collectionView.reloadData()
        self.setCollectionViewHidden()
    }
}

extension AddProductViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height >= 150 {
            textViewHeight.constant = 150
            return
        }
        if textView.contentSize.height <= 50 {
            textViewHeight.constant = 50
            return
        }
        self.setupAutosizeTextView()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
                textView.text = "Placeholder"
                textView.textColor = UIColor.lightGray
        }
    }
}
