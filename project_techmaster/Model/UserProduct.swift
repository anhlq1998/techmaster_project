//
//  UserProduct.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/11/21.
//

import Foundation

class UserProduct {
    var idproduct : String = ""
    var name : String = ""
    var description : String = ""
    var price : String = ""
    var image : [String] = []
    var type : String = ""
    var idWriter:String = ""
    
    init(_ dict: [String:Any]){
        self.idproduct = dict["idproduct"] as! String
        self.name = dict["name"] as! String
        self.description = dict["description"] as! String
        self.price = dict["price"] as! String
        self.image = dict["image"] as! [String]
        self.type = dict["type"] as! String
        self.idWriter = dict["idWriter"] as! String
    }
    
    init(idproduct:String,name:String,description:String,price:String,image:[String],type:String,idWriter:String) {
        self.idproduct = idproduct
        self.name = name
        self.description = description
        self.price = price
        self.image = image
        self.type = type
        self.idWriter = idWriter
    }
    
    func toDictionnary() -> [String:Any]{
        var dic = [String:Any]()
        dic["idproduct"] = self.idproduct
        dic["name"] = self.name
        dic["description"] = self.description
        dic["price"] = self.price
        dic["image"] = self.image
        dic["type"] = self.type
        dic["idWriter"] = self.idWriter
        
        return dic
    }
    
}
