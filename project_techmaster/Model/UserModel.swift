//
//  UserModel.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/31/20.
//

import Foundation
class UserModel : NSObject,NSCoding {
    
    
    var id:String = ""
    var name:String = ""
    var age:String = ""
    var phone:String = ""
    var email:String = ""
    var gender:String = ""
    var avatar:String = ""
    
    override init() {
        
    }
    
    init(id:String,age:String,phone:String,email:String,gender:String,name:String,avatar:String = "") {
        self.id = id
        self.name = name
        self.age = age
        self.phone = phone
        self.email = email
        self.gender = gender
        self.avatar = avatar
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(age, forKey: "age")
        coder.encode(phone, forKey: "phone")
        coder.encode(email, forKey: "email")
        coder.encode(gender, forKey: "gender")
        coder.encode(name, forKey: "name")
        coder.encode(avatar, forKey: "avatar")
        
    }
    
    required init?(coder: NSCoder) {
        id = coder.decodeObject(forKey: "id") as! String
        age = coder.decodeObject(forKey: "age") as! String
        phone = coder.decodeObject(forKey: "phone") as! String
        email = coder.decodeObject(forKey: "email") as! String
        gender = coder.decodeObject(forKey: "gender") as! String
        name = coder.decodeObject(forKey: "name") as! String
        avatar = coder.decodeObject(forKey: "avatar") as? String ?? ""
    }
    
    func dictionary() -> [String:Any]{
        var dic: [String:Any] = [String:Any]()
        dic["id"] = self.id
        dic["age"] = self.age
        dic["phone"] = self.phone
        dic["email"] = self.email
        dic["gender"] = self.gender
        dic["name"] = self.name
        dic["avatar"] = self.avatar
        return dic
    }
}
