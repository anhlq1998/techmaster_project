//
//  ProductModel.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/5/21.
//

import Foundation
import UIKit
class ProductModel {
    var name : String = ""
    var description : String = ""
    var price : String = ""
    var favorite : [String] = []
    var image : [String] = []
    var type : String = ""
    var idproduct : String = ""
    var message : [[String:Any]] = []
    var idWriter : String = ""
    
    init(dict:[String:Any]){
        self.name = dict["name"] as! String
        self.description = dict["description"] as! String
        self.price = dict["price"] as! String
        self.favorite = dict["favorite"] as! [String]
        self.image = dict["image"] as! [String]
        self.type = dict["type"] as! String
        self.idproduct = dict["idproduct"] as! String
        self.message = dict["message"] as! [[String:Any]]
        self.idWriter = dict["idWriter"] as! String
    }
    
    init(
        name:String,
        description:String,
        price:String,
        favorite:[String],
        image:[String],
        type:String,
        idproduct:String,
        message:[[String:Any]],
        idWriter:String
        ) {
        
        
        self.name = name
        self.description = description
        self.price = price
        self.favorite = favorite
        self.image = image
        self.type = type
        self.idproduct = idproduct
        self.message = message
        self.idWriter = idWriter
    }
    
    func toDictionary() -> [String:Any]{
        var dic: [String:Any] = [String:Any]()
        dic["name"] = self.name
        dic["description"] = self.description
        dic["price"] = self.price
        dic["favorite"] = self.favorite
        dic["image"] = self.image
        dic["type"] = self.type
        dic["idproduct"] = self.idproduct
        dic["message"] = self.message
        dic["idWriter"] = self.idWriter
        return dic
    }
    
}
