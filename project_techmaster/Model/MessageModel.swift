//
//  MessageModel.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/6/21.
//

import Foundation
class MessageModel {
    var user : String = ""
    var idUSer : String = ""
    var time : String = ""
    var comment : String = ""
    
    init(dict:[String:Any]) {
        self.user = dict["user"] as! String
        self.time = dict["time"] as! String
        self.comment = dict["comment"] as! String
        self.idUSer = dict["idUSer"] as! String
    }
    
    init(user:String,idUser:String ,time:String ,comment:String) {
        self.user = user
        self.time = time
        self.comment = comment
        self.idUSer = idUser
    }
    
    func todictionary() -> [String:Any]{
        var dic : [String:Any] = [String:Any]()
        dic["user"] = self.user
        dic["time"] = self.time
        dic["comment"] = self.comment
        dic["idUSer"] = self.idUSer
        return dic
    }
}
