//
//  FirebaseAuth.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 1/12/21.
//

import Foundation
import FirebaseAuth
struct FirebaseAuth {
    
    typealias completion = ((ResultFirebase) -> ())
    typealias completionResultUser = ((ResultUser) -> ())
    
    //MARK: LOGIN EMAIL
    //--------------------------------------------------------------------------------------------------------------//
    static func loginEmailPassword(_ email:String, _ password:String, completion : @escaping completion){
        
        Auth.auth().signIn(withEmail: email, password: password) { (auth, err) in
            if err == nil {
                completion(.success)
                return
            }
            completion(.err(err!))
        }
    }
    
    //MARK: REGISTER EMAIL
    //--------------------------------------------------------------------------------------------------------------//
    static func registerEmaiPassword(
        _ id : String ,_ name:String , age:String , _ phone : String , _ gender :String,_ email: String , _ pasword:String,avatar:String = "", completion: @escaping completion){
        Auth.auth().createUser(withEmail: email, password: pasword) { (result, err) in
            if err == nil {
                self.requestUser(id, age, phone, email, gender, name, avatar)
                completion(.success)
                return
            }
            completion(.err(err!))
        }
    }
    
    
    //MARK: SAVE USER AFFTER REGISTER
    //--------------------------------------------------------------------------------------------------------------//
    static func requestUser(_ id : String ,_ age:String, _ phone:String,_ email:String , _ gender: String , _ name:String , _ avatar : String){
        let user = UserModel(id: id, age: age, phone: phone, email: email, gender: gender, name: name, avatar: avatar)
        FirebaseManager.db().collection("user").addDocument(data: user.dictionary())
    }
    
    //MARK: REQUEST USER WHEN LOGIN
    //--------------------------------------------------------------------------------------------------------------//
    static func isLoginUser(email:String){
        FirebaseManager.db().collection("user").getDocuments { (query, err) in
            guard let q = query else {return}
            for qe in q.documents {
                guard let user = qe.data() as? [String:Any] else {return}
//                print(user["email"])
                if user["email"] as! String == email {
                    let user = WebService.parseUser(dict: user)
                    LocalDataProvider.saveUser(user: user)
                    LocalDataProvider.saveToken(token: user.id)
                    
                }
            }
            
        }
    }
    //MARK: UPDATE USER
    //--------------------------------------------------------------------------------------------------------------//
    static func updateUser(name:String,age:String,phone:String,gender:String,avatar:String,completion : @escaping completion){
        let token = LocalDataProvider.getUser().id
        let userInfo = ["name":name,"age":age,"phone":phone,"gender":gender,"avatar":avatar]
        FirebaseManager.db().collection("user").getDocuments { (query, err) in
            guard let q = query else {return}
            for i in q.documents {
                guard let user = i.data() as? [String:Any] else{return}
                if user["id"] as! String == token {
                    FirebaseManager.db().collection("user").document(i.documentID).updateData(userInfo) { (err) in
                        if err == nil {
                            completion(.success)
                        }else{
                            completion(.err(err!))
                        }
                    }
                }
            }
        }
    }
    //MARK: GET INFO USER
    //--------------------------------------------------------------------------------------------------------------//
    static func getUserShop(idWriter:String,completion: @escaping completionResultUser){
        FirebaseManager.db().collection("user").getDocuments { (query, err) in
            guard let q = query else {completion(.err(err!)); return}
            for qe in q.documents {
                guard let user = qe.data() as? [String:Any] else {return}
//                print(user["email"])
                if user["id"] as! String == idWriter {
                    let user = WebService.parseUser(dict: user)
                    completion(.success(user))


                }
            }
            
            
        }
        
        
    }
    //MARK: LOGOUT
    //--------------------------------------------------------------------------------------------------------------//
    static func logout(){
        do {
            try Auth.auth().signOut()
        } catch let err {
            print(err.localizedDescription)
        }
    }
    
    
    
}
