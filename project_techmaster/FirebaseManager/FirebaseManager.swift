//
//  FirebaseManager.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import Foundation
import Firebase
import GoogleSignIn
import FirebaseFirestore
import FirebaseStorage



struct FirebaseManager {
    
    typealias completion = ((ResultFirebase) -> ())
    
    typealias completionResultProduct = ((ResulProduct)-> ())
    
    typealias completionResultUrlImage = ((ResultImageUrl)-> ())
    
    typealias completionResultMyShop  = ((ResultShop) -> ())
    
    
    static var products = [ProductModel]()
    
    
    
    static func db() -> Firestore {
        return Firestore.firestore()
    }
    
    // Firebase DB
    
    static func getProduct(completion: @escaping completionResultProduct){
        FirebaseManager.db().collection("product").getDocuments { (querySnap, err) in
            var products = [ProductModel]()
            
            guard let q = querySnap else { completion(.err(err!));  return }
            for qs in q.documents {
                guard let product = qs.data() as? [String:Any] else {return}
                let pd = ProductModel(dict: product)
                products.append(pd)
            }
            completion(.success(products))
        }
    }
    
    static func fetchProduct(completion : @escaping completion){
        var listImage = [
            "https://firebasestorage.googleapis.com/v0/b/projecttechmaster.appspot.com/o/h-231083.jpg?alt=media&token=5cea8377-4e78-43dc-b1fc-52464712f1d0",
            
            "https://www.sapo.vn/blog/wp-content/uploads/2016/01/8-meo-de-xay-dung-thuong-hieu-shop-my-pham-online-khi-moi-kinh-doanh-p1-3.jpg",
            
            "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg",
            "https://1.bp.blogspot.com/-wN1tO-lDV7Q/XVOa6eoUxuI/AAAAAAAAEo4/TOunleahOrIUAohAWi2gx5CVj91qeYs7gCLcBGAs/s1600/chup-anh-san-pham-chuyen-nghiep-hoi-an-2.jpg",
            "https://logoaz.net/wp-content/uploads/2019/02/quy-tac-chup-anh-san-pham-phong-nen.jpg",
            "https://chupanhsanpham.com.vn/wp-content/uploads/2017/11/le-fruit-passion-2.jpg",
            "https://rgb.vn/ideas/wp-content/uploads/2016/06/rgb.vn_le-fruit-packeging_18.jpg"
        ]
        listImage.shuffle()
        
        
        let types = [TypeProduct.new.rawValue,TypeProduct.now.rawValue]
        
        let listProduct = ["Iphone 11 ", "Iphone 12", "iphone 12 Promax", "Samsung s9" , "Macbook pro" , "ipad"]
        
        for _ in 0...5 {
            
            let idProduct = UUID().uuidString
            
            let localData = LocalDataProvider.getUser()
            
            let message = MessageModel(
                user: "\(localData.name)",
                idUser: "\(localData.id)",
                time: "10101998",
                comment: "demo coment"
            )
            
            let product = ProductModel(
                name: "\(listProduct[Int.random(in: 0...listProduct.count - 1)])",
                description: "Set Son MAC Travel Exclusive Powder Kiss Lipstick",
                price: "\(Int.random(in: 120...200))",
                favorite: [],
                image: listImage.shuffled(),
                type: types[Int.random(in: 0..<types.count)],
                idproduct: idProduct,
                message: [message.todictionary()],
                idWriter: "1"
            )
            
//            FirebaseManager.db().collection("product").addDocument(data: product.toDictionary())
            FirebaseManager.db().collection("product").addDocument(data: product.toDictionary()) { (err) in
                if err == nil {
                    completion(.success)
                    return
                }
                completion(.err(err!))
            }
        }


    }
    
    static func upLoadImage(image:UIImage,filePath: String = UUID().uuidString ,completion: @escaping completionResultUrlImage){
        let filePath = filePath
        let storageRef = Storage.storage().reference()
        var data = NSData()
        data = image.jpegData(compressionQuality: 0.8)! as NSData
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        
        
        storageRef.child(filePath).putData(data as Data, metadata: metaData) { (meta, err) in
            if err == nil {
                print("Meta data \(meta!)")
                storageRef.child(filePath).downloadURL { (url, errImage) in
                    if err == nil {
                        print("URL \(url!.absoluteString)")
                        completion(.success(url!))
                        return
                    }
                    print("DOWNLOAD IMAGE ERR : \(errImage!)")
                    completion(.err(errImage as! Error))
                }
             return
            }
            print("ERRMETA \(err!.localizedDescription)")
            completion(.err(err as! Error))
        }
        
    }
    
    
    static func createProduct(name:String,description:String,price:String,image:[String],type:String,completion:@escaping completion){
        let id = UUID().uuidString
        let userProduct = UserProduct(idproduct: id, name: name, description: description, price: price, image: image, type: type, idWriter: LocalDataProvider.getUser().id)
        FirebaseManager.db().collection("user_product").addDocument(data: userProduct.toDictionnary()) { (err) in
            if err == nil {
                print("add Item succes")
                FirebaseManager.requestProduct(name:name , description: description, price: price, favorite: [], image: image, type: type, idproduct: id, message: [], idWriter: LocalDataProvider.getUser().id)
                completion(.success)
                return
            }
            print("ERR\(err?.localizedDescription)")
            completion(.err(err as! Error))
        }
        
    }
    
    static func requestProduct(name:String,description:String,price:String,favorite:[String],image:[String],type:String,idproduct:String,message:[[String:Any]],idWriter:String){
        let product = ProductModel(name: name, description: description, price: price, favorite: favorite, image: image, type: type, idproduct: idproduct, message: message, idWriter: idWriter)
        FirebaseManager.db().collection("product").addDocument(data: product.toDictionary()) { (err) in
            if err == nil {
                print("request product succes")
                NotificationCenter.default.post(name: .noti_request_product_success, object: nil)
                return
            }
            print("ERR \(err?.localizedDescription)")
        }
        
    }
    
    static func getMyShop(completion: @escaping completionResultMyShop){
        FirebaseManager.db().collection("user_product").getDocuments { (query, err) in
            if err == nil {
                var products = [UserProduct]()
                for i in query!.documents {
                    guard let product = i.data() as? [String : Any] else {return}
                    let idWriter = product["idWriter"] as! String
                    if idWriter  == LocalDataProvider.getUser().id {
                        let product = UserProduct(product)
                        products.append(product)
                    }
                }
                completion(.success(products))
                return
            }
            
            print("ERR REQUEST \(err?.localizedDescription)")
            completion(.err(err as! Error))
        }
    }
    
    static func writeMessage(comment:String,idProduct:String){
        let comment = MessageModel(user: "user", idUser: "idUser", time: "time", comment: comment.trimmingCharacters(in: .whitespacesAndNewlines))
        let message = [comment.todictionary()]
        
        
    }
    
    
    
    
}
