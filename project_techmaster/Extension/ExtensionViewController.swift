//
//  ExtensionViewController.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import Foundation
import UIKit
import Toast_Swift
extension UIViewController{
    
    func setCustomNavigationbar(leftTitle:String = "" ,action:Selector = #selector(goBackButtonNavigation)){
           self.navigationItem.setHidesBackButton(true, animated:false)
           navigationController?.navigationBar.isTranslucent = false
           let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 40))
           let imageView = UIImageView(frame: CGRect(x: 0, y: 12, width: 20, height: 20))
           let leftTitleLabel = UILabel()
           leftTitleLabel.text = leftTitle
           leftTitleLabel.sizeToFit()
           leftTitleLabel.isUserInteractionEnabled = true
           
           let leftLabel = UIBarButtonItem(customView: leftTitleLabel)
           imageView.contentMode = .scaleAspectFit
           if let imgBackArrow = UIImage(named: "ic_back") {
               imageView.image = imgBackArrow
           }
           view.addSubview(imageView)
           let backTap = UITapGestureRecognizer(target: self, action:action)
           view.addGestureRecognizer(backTap)
           leftTitleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: action))
           
           let leftBarButtonItem = UIBarButtonItem(customView: view )
           self.navigationItem.leftBarButtonItems = [leftBarButtonItem,leftLabel]
       }
       
       @objc func goBackButtonNavigation(){
           self.navigationController?.popViewController(animated: true)
       }
    
     func showAlert(title:String,message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    func showCustomAlert(title:String,message:String,titleAction:String,action: @escaping () -> (Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancleAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        let okeAction = UIAlertAction(title: titleAction, style: .default) { (_) in
            action()
        }
        alert.addAction(cancleAction)
        alert.addAction(okeAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    func showToast(text:String,position:ToastPosition = .top){
        self.view.hideToast()
        self.view.makeToast("\(text)", duration: 1.5, position: position)
    }
    
}

extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}

extension NSNotification.Name {
    static let noti_get_products = Notification.Name("noti_get_products")
    static let noti_request_product_success  = Notification.Name("noti_request_product_success")
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
