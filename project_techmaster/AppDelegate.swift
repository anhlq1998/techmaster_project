//
//  AppDelegate.swift
//  project_techmaster
//
//  Created by Le Quang Anh on 12/30/20.
//

import UIKit

import Firebase
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        AppDelegate.getListProduct()
//        FirebaseManager.fetchProduct { (result) in
//            switch result {
//            case .success:
//                print("SUCCES")
//            case .err(let err):
//                print(err)
//            }
//        }
        window = UIWindow(frame: UIScreen.main.bounds)
        settingMain()
        return true
    }

    func settingMain(){
        let token = LocalDataProvider.token()
        print("TOKEN \(token)")
        if token.isEmpty {
            gotoLogin()
        }else{
            gotoHome()
        }
    }
    
    func gotoLogin(){
        print("LOGIN")
        let vc = LoginViewController()
        let nav = UINavigationController(rootViewController: vc)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }
    
    func gotoHome(){
        print("HOME")
        let vc = TabbarViewController()
        let nav = UINavigationController(rootViewController: vc)
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
    
   static func getListProduct(){
        FirebaseManager.getProduct { (result) in
            switch result {
            case .success(let product):
                FirebaseManager.products = product
                NotificationCenter.default.post(name: .noti_get_products, object: nil)
                break
            case .err(let err):
                break
            }
        }
    }

}

